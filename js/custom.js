jQuery(document).ready(function($) { 
var inputSearch = $('.search input');

/*----------Reload list----------*/

	$('#reload').on('click', function(){
		$('.persons-info').remove(); 
		$('#go-to-the-main').hide();
		$('#information').hide();
		inputSearch.val('');		
		sendAjax();
	});

/*-------------------------------*/ 

	$('#reload').trigger('click');
	var users_names = [];

/*----------- Search ------------*/

	$('.search').on('click','button', function(event){
		event.preventDefault(); 
		$('#go-to-the-main').show();

		var personsInfoRows = document.getElementsByClassName('persons-info'),
			info = document.getElementById('information'),
		    inputValue = inputSearch.val().toLowerCase(),
		    count = 0;

		    $('.persons-info').hide();
		$.each(users_names, function(index, value) { 
			if(value.indexOf(inputValue) >= 0 && inputValue.length > 0) {
				 $('#information').hide();
				 personsInfoRows[index].style.display = 'block'; 
			} 
			if(value.indexOf(inputValue) < 0) { 
			   count++;
			   if (count == users_names.length) {	
				 $('.text-attention').text("Sorry, keep going to search!"); 
				 $('#information').css('display','flex');
				}
			} 
			if(inputValue.length == 0 ) {
				$('.text-attention').text("Please enter first name in the field above."); 
				$('#information').css('display','flex');
			} 
		});
	});

	$('#go-to-the-main').click(function() {
		$(this).hide();
		$('#information').hide();
	    $('.persons-info').show();
		inputSearch.val('');
	});

/*-------------------------------*/
/*------- Accordion work --------*/
 
    function close_full_info() {
        $('.show-more').removeClass('active');
        $('.full-info').slideUp(300).removeClass('open');
    }
 
    $('#container-of-users').on("click",'.accordionTrigger', function(e) {
        var fullInfo = $(this).parent().find('.full-info'),
        	showMoreIcon = $(this).find('.show-more');
 
        if(showMoreIcon.is('.active')) {
            close_full_info();
        }else {
            close_full_info(); 
            showMoreIcon.addClass('active');
            // Open up the hidden content panel
            fullInfo.slideDown(300).addClass('open'); 
        }   
    }); 

/*-------------------------------*/
/*-------- Chart of gender-------*/

	 function initChart(percentMale, percentFemale) {
		Highcharts.chart('chart-of-gender', {
		    chart: {
		        type: 'pie'
		    },
		    title: {
		        text: 'Gender users'
		    }, 
		    plotOptions: {
		        series: {
		            dataLabels: {
		                enabled: true,
		                format: '{point.name}: {point.y:.1f}%'
		            }
		        }
		    },

		    tooltip: {
		        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
		        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
		    },
		    series: [{
		        name: 'Gender',
		        colorByPoint: true,
		        data: [{
		            name: 'Male',
		            y: percentMale,
		            drilldown: 'Male'
		        }, {
		            name: 'Female',
		            y: percentFemale,
		            drilldown: 'Female'
		        }]
		    }]
		});
	};

/*-------------------------------*/
/*-------Show / hide popup-------*/
 
	$('.button-show-chart').click(function() {
		$('.popup-chart').addClass('visible');
	});

	$('.close-button').click(function() {
		$('.popup-chart').removeClass('visible');
	});

/*-------------------------------*/ 
/*------Work with randomuser-----*/

	function sendAjax() {
		$.ajax({
		  url: 'https://randomuser.me/api/?results=30&exc=nat',
		  dataType: 'json',
		  success: function(data) {
		    initialize_user(data);
		  }
		}); 
	}

	function initialize_user(data) { 
		var count_users = data.results.length, 
			count_male = 0, count_female = 0;
			users_names = [];
		for(var i = 0; i < count_users; i++) {
			users_names.push(data.results[i].name.first);
			(data.results[i].gender === 'male') ? count_male++ : count_female++;	
			 var div = `
	 				<div class="persons-info">
		  				<div class="short-info accordionTrigger">
		  					<div class="photo">
		  						<div class="img-wrapper"><img src="`+data.results[i].picture.medium+`" alt="photo-person"></div>
		  					</div>
			  				<div class="last-name">`+data.results[i].name.last+`</div>
			  				<div class="first-name">`+data.results[i].name.first+`</div>
			  				<div class="username">`+data.results[i].login.username+`</div>
			  				<div class="phone">`+data.results[i].phone+`</div>
			  				<div class="location">`+data.results[i].location.state+`</div>
			  				<div class="show-more"><div><span></span><span></span></div></div>
		  				</div>
		  				<div class="full-info">
		  					<div class="name">`+data.results[i].name.first+`<span class="img-of-gender `+data.results[i].gender+`"></span></div>
				            <div class="profile-photo">
				              <div class="img-wrapper"><img src="`+data.results[i].picture.large+`" alt="profile-photo"></div>
				            </div>
		  					<div class="data-columns">
		  						<div class="username">
		  							<span class="title">Username</span>
		  							<span class="value">`+data.results[i].login.username+`</span>
		  						</div>

				                <div class="address">
				                  <span class="title">Address</span>
				                  <span class="value">`+data.results[i].location.street+`</span>
				                </div>

				                <div class="birthday">
				                  <span class="title">Birthday</span>
				                  <span class="value">`+data.results[i].dob.substr(0,10)+`</span>
				                </div>

		  						<div class="registered">
		  							<span class="title">Registered</span>
		  							<span class="value">`+data.results[i].registered.substr(0,10)+`</span>
		  						</div>

				                <div class="city">
				                  <span class="title">City</span>
				                  <span class="value">`+data.results[i].location.city+`</span>
				                </div> 

				                <div class="phone">
				                  <span class="title">Phone</span>
				                  <span class="value">`+data.results[i].phone+`</span>
				                </div>

		  						<div class="email">
		  							<span class="title">Email</span>
		  							<span class="value">`+data.results[i].email+`</span>
		  						</div>

		  						<div class="zp-code">
		  							<span class="title">Zip Code</span>
		  							<span class="value">`+data.results[i].location.postcode+`</span>
		  						</div>

		  						<div class="cell">
		  							<span class="title">Cell</span>
		  							<span class="value">`+data.results[i].cell+`</span>
		  						</div> 
		  					</div>

		  				</div>  				
		  			</div>
		  			`
		  	$('#container-of-users').append(div); 
		}
		jQuery('#preloader').fadeOut('700');  
		//reckon ratio 
	  	var percentMale = (count_male/count_users)*100,
	  		percentFemale = 100 - percentMale;  

	  	// Create chart
  		initChart(percentMale, percentFemale); 
	} 

/*-------------------------------*/

});
